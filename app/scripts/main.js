$(document).ready(function() {

  $('.phone').inputmask('+7(999)-999-99-99');

  $('.open-modal').on('click', function(e) {
    e.preventDefault();
    $('.t-modal').toggle();
  });

  $('.t-modal__close').on('click', function(e) {
    e.preventDefault();
    $('.t-modal').toggle();
  });

  $('.t-header__toggle').on('click', function(e) {
    e.preventDefault();
    $('.t-header__info').slideToggle('fast');
    $('.t-header__nav').slideToggle('fast');
  });

  new Swiper('.t-win .swiper-container', {
    navigation: {
      nextEl: '.t-win .swiper-button-next',
      prevEl: '.t-win .swiper-button-prev'
    },
  });

  new Swiper('.t-home-reviews .swiper-container', {
    slidesPerView: 2,
    navigation: {
      nextEl: '.t-home-reviews .swiper-button-next',
      prevEl: '.t-home-reviews .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });

  new Swiper('.t-home-big .swiper-container', {
    slidesPerView: 1,
    navigation: {
      nextEl: '.t-home-big .swiper-button-next',
      prevEl: '.t-home-big .swiper-button-prev',
    },
    autoplay: {
      delay: 5000,
    },
    spaceBetween: 15,
    loop: true
  });

  new Swiper('.t-home-reviews_page .swiper-container', {
    slidesPerView: 1,
    navigation: {
      nextEl: '.t-home-reviews_page .swiper-button-next',
      prevEl: '.t-home-reviews_page .swiper-button-prev',
    }
  });

  /* new Swiper('.t-home-projects__cards', {
    pagination: {
      el: '.t-home-projects__cards .swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.t-home-projects__cards .swiper-button-next',
      prevEl: '.t-home-projects__cards .swiper-button-prev',
    },
    spaceBetween: 30
  }); */

  new Swiper('.t-home-papers__cards', {
    slidesPerView: 4,
    spaceBetween: 40,
    pagination: {
      el: '.t-home-papers .swiper-pagination'
    },
    navigation: {
      nextEl: '.t-home-papers .swiper-button-next',
      prevEl: '.t-home-papers .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 1,
        spaceBetween: 40,
      }
    }
  });

});